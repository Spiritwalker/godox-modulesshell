﻿using GodOx.Auth.API.Infrastructure.Configs;
using GodOx.Auth.API.Controllers;
using GodOx.Mall.API.Models.Dtos.Input;
using GodOx.Mall.API.Models.Entity;
using GodOx.Share.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

/*************************************
* 类名：AppUserController
* 作者：realyrare
* 邮箱：mahonggang8888@126.com
* 时间：2021/8/23 11:01:15
*┌───────────────────────────────────┐　    
*│　   版权所有：神牛软件　　　　	     │
*└───────────────────────────────────┘
**************************************/

namespace GodOx.Mall.API.Controllers
{
    /// <summary>
    /// 小程序用户地址
    /// </summary>
    public class AppUserAddressController : AppBaseController
    {

        private readonly IBaseServer<AppUserAddress> _appUserAddressService;
        private readonly DbContext _dbContext;
        public AppUserAddressController(IBaseServer<AppUserAddress> appUserAddressService, DbContext dbContext)
        {
            _appUserAddressService = appUserAddressService;
            _dbContext = dbContext;
        }

        private Tuple<string, string, string> GetNamesBySplitRegions(string regionName)
        {
            if (string.IsNullOrEmpty(regionName))
            {
                throw new ArgumentNullException("用户省市区为空!");
            }
            var regionsArry = regionName.Split(',', StringSplitOptions.RemoveEmptyEntries);
            return new Tuple<string, string, string>(regionsArry[0], regionsArry[1], regionsArry[2]);
        }
        [HttpGet]
        public async Task<ApiResult> Lists()
        {
            var addressList = await _appUserAddressService.GetListAsync(d => d.IsDeleted == false && d.AppUserId == HttpWx.AppUserId);
            return new ApiResult(addressList);
        }
        [HttpGet]
        public async Task<ApiResult> Detail(int addressId)
        {
            var addressModel = await _appUserAddressService.GetModelAsync(d => d.IsDeleted == false && d.AppUserId == HttpWx.AppUserId && d.Id.Equals(addressId));
            return new ApiResult(addressModel);
        }
        [HttpPost]
        public async Task<ApiResult> Add([FromForm] AddressInput input)
        {
            var tupleValue = GetNamesBySplitRegions(input.Region);
            var createModel = AppUserAddress.Create(HttpWx.AppUserId, input.Name, input.Detail, input.Phone, tupleValue.Item1, tupleValue.Item2, tupleValue.Item3);
           
            var sign = await _appUserAddressService.AddAsync(createModel);
            return Result(sign);
        }
        [HttpPost]
        public async Task<ApiResult> Edit([FromForm] AddressInput input)
        {
            var tupleValue = GetNamesBySplitRegions(input.Region);
           var modifyModel= new AppUserAddress();
            modifyModel.Modify(input.Name, input.Detail, tupleValue.Item1, tupleValue.Item2, tupleValue.Item3, input.Phone);
            var sign = await _appUserAddressService.UpdateAsync(d => modifyModel, d => d.IsDeleted == false && d.Id == input.AddressId && d.AppUserId == HttpWx.AppUserId);
            return Result(sign);
        }

        [HttpPost]
        public async Task<ApiResult> Delete([FromForm] int addressId)
        {
            var model = new AppUserAddress();
            model.SoftDelete();
            var sign = await _appUserAddressService.UpdateAsync(d => model, d => d.Id == addressId);
            return new ApiResult(sign);
        }

        [HttpPost]
        public async Task<ApiResult> SetIsDefault([FromForm] int addressId)
        {
            //把有默认地址的取消
            try
            {
                _dbContext.Db.BeginTran();
                var appUserAddress1 = new AppUserAddress(); appUserAddress1.SetDefaultAddress(false);
                var result1 = await _dbContext.Db.Updateable<AppUserAddress>().SetColumns(d => appUserAddress1).Where(d => d.IsDeleted && d.AppUserId == HttpWx.AppUserId).ExecuteCommandAsync();

                //设置当前新的默认地址
                var appUserAddress2 = new AppUserAddress(); appUserAddress1.SetDefaultAddress(true);
                var result2 = await _dbContext.Db.Updateable<AppUserAddress>().SetColumns(d => appUserAddress2).Where(d => d.IsDeleted && d.AppUserId == HttpWx.AppUserId && d.Id == addressId).ExecuteCommandAsync();
                if (result1 > 0 && result2 > 0)
                {
                    _dbContext.Db.CommitTran();
                }
            }
            catch
            {
                _dbContext.Db.RollbackTran();
                return new ApiResult("地址设置失败！");
            }
            return new ApiResult();
        }
    }
}