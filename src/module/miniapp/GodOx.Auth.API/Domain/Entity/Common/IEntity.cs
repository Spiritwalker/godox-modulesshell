﻿namespace GodOx.Auth.API.Domain.Entity.Common
{
    public interface IEntity
    {
        int Id { get;  }
    }
}
