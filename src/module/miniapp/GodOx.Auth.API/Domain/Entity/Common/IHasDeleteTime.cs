﻿using System;

namespace GodOx.Auth.API.Domain.Entity.Common
{
    public interface IHasDeleteTime
    {
        DateTime? DeleteTime { get; }
    }
}
