﻿using System;

namespace GodOx.Auth.API.Domain.Entity.Common
{
    public interface IHasModifyTime
    {
        DateTime? ModifyTime { get;  }
    }
}
