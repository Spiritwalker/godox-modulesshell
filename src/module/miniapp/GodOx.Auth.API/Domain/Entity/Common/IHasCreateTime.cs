﻿using System;

namespace GodOx.Auth.API.Domain.Entity.Common
{
    public interface IHasCreateTime
    {
        DateTime CreateTime { get; }
    }
}
