﻿namespace GodOx.Auth.API.Infrastructure.Configs
{
    public class AppConfig
    {
        public static string AppId { get; set; }
        public static string AppSecret { get; set; }
    }
}
